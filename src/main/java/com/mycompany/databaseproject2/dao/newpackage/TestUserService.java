/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject2.dao.newpackage;

import com.mycompany.databaseproject2.dao.service.UserService;
import com.mycompany.databaseproject2.model.User;

/**
 *
 * @author Arthi
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("muangsiri", "password");
        if(user!=null){
            System.out.println("Welcome : "+user.getName());
        }
    }
}
