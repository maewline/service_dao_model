/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject2.dao.service;

import com.mycompany.databaseproject2.dao.UserDao;
import com.mycompany.databaseproject2.model.User;

/**
 *
 * @author Arthi
 */
public class UserService {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user=  userDao.getByName(name);
        try{
        if(user!=null&&user.getPassword().equals(password)){
            return user;
        }
        }catch(Exception ex){
            System.out.println("error");
        }
        return null;
    }
}
